/**
 * @Unit    : 
 * @Version : 1.0
 * @Author  : 靳普诏
 * @Brief   : 简单介绍
 */

#include "az_log.h"
#include <stdarg.h>
#include <stdio.h>



typedef int (* azLogPutChars)(const char *buff, int size);


int azLogPutCharsDefault(const char *buff, int size)
{
    return 0;
}

static azLogPutChars g_az_log_fcn = azLogPutCharsDefault;


int azLogF(const char *format, ...)
{
    int result = 0;
    
    if (g_az_log_fcn != NULL)
    {
        char temp[512] = 0;

        va_list args;
        // const char *args1;
        va_start(args, format);
        // args1 = va_arg(args, const char *);
        result = vsnprintf(temp, sizeof(temp), format, args);
        va_end(args);

        result = g_az_log_fcn(temp, result);
    }
    
    return result;
}

