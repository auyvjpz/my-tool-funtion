#include "mytools.h"
#include "string.h"

/**
 * @brief string type to integer
 * @param str 
 * @return int 
 */
int __str2int__(const char *str)
{
    int result = 0;
    char fu = 0;
    char *p = (char *)str;
    if (*p == '-')
    {
        fu = 1;
        p++;
    }
    while((*p != '\0') && (*p >= '0') && (*p <= '9'))
    {
        result = result * 10 + (*p - '0');
        p++;
    }
    if (fu == 1)
    {
        result = -result;
    }
    return result;
}

/**
 * @brief string type to double float
 * @param str 
 * @return double 
 */
double __str2real__(const char *str)
{
    double result = 0.0;
    char fu = 1;
    char *p = (char *)str;
    double pot = 10;

    if (*p == '-')
    {
        fu = -1;
        p++;
    }
    while((*p != '\0') && (*p >= '0') && (*p <= '9'))
    {
        result = result * 10 + (*p - '0');
        p++;
    }
    if (*p != '.')
    {        
        return result * fu;
    }
    p++;
    while((*p != '\0') && (*p >= '0') && (*p <= '9'))
    {
        result = result + (*p - '0') / pot;
        pot *= 10;
        p++;
    }
    return result * fu;
}










