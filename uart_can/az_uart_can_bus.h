/**
 * 
 * @File    : 
 * @Brief   : 用于uart和can数据的转换发送
 * @Version : 1.0
 * @Author  : 靳普诏
 * @E-Mail  : 499367619@qq.com
 */


#ifndef _AZ_UART_CAN_BUS_H_
#define _AZ_UART_CAN_BUS_H_



typedef int TAzDoUartWrite(int channel, const char *data, int len);

typedef int TAzDoUartRead(int channel, char *data, int len);




typedef struct TAzCanDataTag TAzCanData;
struct TAzCanDataTag
{
    unsigned int id;   ///< can id
    char type;   ///< 0 标准帧  1 扩展帧
    char dlc;   ///< can数据长度
    char data[8];   /// 数据场
};

typedef int TAzDoCanWrite(int channel, const TAzCanData *data, int cnt);

typedef int TAzDoCanRead(int channel, TAzCanData *data, int cnt);


struct TAzUartCanConvertTag
{
    char can_channel;   ///< 标记can通道
    TAzCanData  can_data;     ///< 串口向can发送时的 can_id

    TAzDoUartWrite WriteDataToUart;
    TAzDoUartRead ReadDataFromUart;

    TAzDoCanWrite WriteDataToCan;
    TAzDoCanRead ReadDataFromCan;
};







#endif






